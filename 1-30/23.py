"""
 Write a Python program to replace whitespaces with an underscore and vice versa.
"""

import re

s = "hello_this is karan kanwal 09_and_I am from_haldwani"

x = re.sub("_","!",s)
x = re.sub(" ","_",x)
x = re.sub("!"," ",x)
print(x)