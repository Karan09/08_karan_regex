"""
Write a Python program to check that a string contains only a certain set of characters (in this case a-z, A-Z and 0-9).
"""

import re

s = input("Enter your string: ")

c = re.compile(r'[a-zA-Z0-9]')
f = c.search(s)
print(bool(f))
