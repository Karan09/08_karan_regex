"""
Write a Python program to check for a number at the end of a string
"""

import re
s = input("Enter your string: ")

if bool(re.search("2$",s)):
    print(True)
else:
    print(False)