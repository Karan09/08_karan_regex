"""
Write a Python program that matches a string that has an a followed by two to three 'b'.
"""

import re
s = input("Enter your string: ")

if  bool(re.search("ab{2,3}?",s)) and not bool(re.search("ab{4}",s)):
    print(True)
else:
    print(False)