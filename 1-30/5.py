"""
Write a Python program that matches a string that has an a followed by three 'b'.
"""

import re
s = input("Enter your string: ")

if bool(re.search("ab{3}",s)):
    print(True)
else:
    print(False)