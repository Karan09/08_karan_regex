"""
Write a Python program where a string will start with a specific number.
"""

import re
s = input("Enter your string: ")

if bool(re.search("^2",s)):
    print(True)
else:
    print(False)