"""
Write a Python program to find the occurrence and position of the substrings within a string.
"""

import re

s = "Python exercises, PHP exercises, C# exercises"

for match in re.finditer("exercises",s):
    a = match.start()
    b = match.end()
    print('Found at %d:%d' % (a,b) 