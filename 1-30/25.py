"""
Write a Python program to convert a date of yyyy-mm-dd format to dd-mm-yyyy format.
"""
import re
date = "2018-12-28"

x = re.sub('(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1',date)
print(x)